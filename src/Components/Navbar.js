import {Link} from 'react-router-dom'
import {useState} from 'react'
import logo from '../image2vector.svg'
const Navbar = () => {
    const navStyle = {
        color: '#ff4f5a',
        textDecoration: 'none'
    }
    const [statusham, setStatusHam] = useState(false);
    const handleClick = () => setStatusHam(!statusham)
    return (
        <div className="nav-bar">
            {/* <h1><Link to="/" style={navStyle}>KaGoom</Link></h1> */}
            <img src={logo} className="logo" />
            {/* <div className="nav-item-container" > */}
            <ul className={statusham ? "nav-items active" : "nav-items"}>
                <li className="item"><Link to="/" style={navStyle}>Home</Link></li>
                <li className="item"><Link to="/about" style={navStyle}>About</Link></li>
                <li className="item"><Link to="/singin" style={navStyle}>Sign In</Link></li>
            </ul>
            <div className="burger" onClick={handleClick} >
                <div className="dash"></div>
                <div className="dash"></div>
                <div className="dash"></div>
            </div>
        </div>
    )
}

export default Navbar
