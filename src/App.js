import './App.css';
import Navbar from './Components/Navbar'
import backgr from './3255337.jpg'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'

function App() {
  return (
    <Router>
      <div className="App" >
        <Navbar />
        <img src={backgr} className="background" />
      </div>
    </Router>
  );
}

export default App;
